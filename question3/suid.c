#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    printf("EUID: %d \n", geteuid());
    printf("EGID: %d \n", getegid());
    printf("RUID: %d \n", getuid());
    printf("RGID: %d \n", getgid());

    FILE *f;
    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    printf("Hello world\n");
    f = fopen(argv[1], "r");
    
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    
    printf("File opens correctly\n");
    fclose(f);
    
    exit(EXIT_SUCCESS);
    return 0;
}
