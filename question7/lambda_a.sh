#!/bin/bash

# Modify file
modify() {
	echo "lambda_a new text" >> $1
}

# Rename file
rename() {
	mv $1 $1.new
}

# Delete file
delete() {
	rm -f $1
}

# Delete or Rename files I'm not Master
not_my() {
	if [ $# == 1 ]
	then
		not_my $1 $(ls -la $1)
	else
		if [ $4 != "lambda_a" ]
		then
			rename $1
			delete $1
		fi
	fi
}

# Courses files
parcours_files() {
	for arg in $@
	do
		if [ $arg != $2 ]
		then
			if [ -d $2$arg ]
			then
				echo "Directory : $arg"
				parcours_files $1 $2$arg/ $(ls $2$arg)
			fi

			if [ -e $2$arg ]
			then
				echo "File : $arg"
				$1 $2$arg
			fi
		fi
	done
}

echo "Peuvent lire tous les fichiers et sous-répertoires contenus dans dir_a et dir_c"
echo "###############################################################################"

parcours_files "cat" "dir_a/" $(ls dir_a/)
parcours files "cat" "dir_c/" $(ls dir_c/)

echo "Peuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer, ni créer des nouveaux fichiers"
echo "###############################################################################"

parcours_files "modify" "dir_c/" $(ls dir_c/)
parcours_files "rename" "dir_c/" $(ls dir_c/)
parcours_files "delete" "dir_c/" $(ls dir_c/)
touch dir_c/lambda_a.file

echo "Peuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_a, et peuvent créer de nouveaux fichiers et répertoires dans dir_a"
echo "###############################################################################"

parcours_files "modify" "dir_a/" $(ls dir_a/)
touch dir_a/lambda_a.file
mkdir dir_a/lambda_a_dir

echo "N’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_a qui ne leur appartiennent pas"
echo "###############################################################################"

parcours_files "not_my" "dir_a/" $(ls dir_a/)

echo "Ne peuvent pas ni lire, ni modifier, ni effacer les fichiers dans dir_b, et ne peuvent pas créer des nouveaux fichiers dans dir_b"
echo "###############################################################################"

parcours_files "cat" "dir_b/" $(ls dir_b/)
parcours_files "modify" "dir_b/" $(ls dir_b/)
parcours_files "delete" "dir_b/" $(ls dir_b/)
touch dir_b/lambda_a.file
