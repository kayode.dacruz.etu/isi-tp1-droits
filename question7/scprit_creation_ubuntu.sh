#!/bin/bash

sudo addgroup admin;

sudo adduser --no-create-home --disabled-password --ingroup admin admin;
sudo addgroup groupe_a && sudo adduser --no-create-home --disabled-password --ingroup groupe_a lambda_a;
sudo addgroup groupe_b && sudo adduser --no-create-home --disabled-password --ingroup groupe_b lambda_b;

## Create folders and set permissions

mkdir dir_a dir_b dir_c \
&& (sudo chown admin:groupe_a dir_a; sudo chown admin:groupe_b dir_b; sudo chown admin: dir_c) \
&& (sudo chmod ug+rwx,o-rwx,g+s,+t dir_a dir_b; sudo chmod a+rwx,o-w dir_c);
