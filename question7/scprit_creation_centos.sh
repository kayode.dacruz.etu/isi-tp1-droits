#!/bin/bash

sudo groupadd admin;

sudo adduser --no-create-home  admin -G admin;
sudo groupadd groupe_a;
sudo adduser --no-create-home   lambda_a -G groupe_a;
sudo groupadd groupe_b; 
sudo adduser --no-create-home   lambda_b -G groupe_b;

## Create folders and set permissions

mkdir dir_a dir_b dir_c \
&& (sudo chown admin:groupe_a dir_a; sudo chown admin:groupe_b dir_b; sudo chown admin: dir_c) \
&& (sudo chmod ug+rwx,o-rwx,g+s,+t dir_a dir_b; sudo chmod a+rwx,o-w dir_c);
