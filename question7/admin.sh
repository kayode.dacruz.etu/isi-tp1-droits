#!/bin/bash

# Modify file
modify() {
	echo "admin new text" >> $1
}

# Rename file
rename() {
	mv $1 $1.new
}

# Delete file
delete() {
	rm -f $1
}

# Delete or Rename files I'm not Master 
not_my() {
	if [ $# == 1 ]
	then
		not_my $1 $(ls -la $1)
	else
		if [ $4 != "lambda_a" ]
		then
			rename $1
			delete $1
		fi
	fi
}

# Course files
parcours_files() {
	for arg in $@
	do
		if [ $arg != $2 ]
		then
			if [ -d $2$arg ]
			then
				echo "Directory : $arg"
				parcours_files $1 $2$arg/ $(ls $2$arg)
			fi

			if [ -e $2$arg ]
			then
				echo "File : $arg"
				$1 $2$arg
			fi
		fi
	done
}

echo "Peut lire tous les fichiers et sous-répertoires contenus dans dir_a, dir_c et dir_b"
echo "###############################################################################"

parcours_files "cat" "dir_a/" $(ls dir_a/)
parcours files "cat" "dir_b/" $(ls dir_b/)
parcours files "cat" "dir_c/" $(ls dir_c/)

echo "Peut lire, modifier, renommer et effacer des fichiers dans dir_c, et peut créer des nouveaux fichiers dans dir_c"
echo "###############################################################################"

parcours_files "modify" "dir_c/" $(ls dir_c/)
parcours_files "rename" "dir_c/" $(ls dir_c/)
parcours_files "delete" "dir_c/" $(ls dir_c/)
touch dir_c/admin.file

echo "Peut modifier tous les fichiers contenus dans l’arborescence à partir de dir_a et dir_b, et peuvent créer de nouveaux fichiers et répertoires dans dir_a et dir_b"
echo "###############################################################################"

parcours_files "modify" "dir_a/" $(ls dir_a/)
touch dir_a/admin.file
mkdir dir_a/admin_dir

parcours_files "modify" "dir_b/" $(ls dir_b/)
touch dir_b/admin.file
mkdir dir_b/admin_dir

echo "A le droit d’effacer, de renommer, des fichiers dans dir_a et dir_b qui ne lui appartient pas"
echo "###############################################################################"

parcours_files "not_my" "dir_a/" $(ls dir_a/)
parcours_files "not_my" "dir_b/" $(ls dir_b/)