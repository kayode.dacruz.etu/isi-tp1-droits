#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>
#include "check_pass.h"

int main(int argc, char** args) {
	if(argc != 3) {
		printf("usage : pwg [password file] [user] \n");
		exit(EXIT_FAILURE);
	}

	int passwd_size = 20;
	char old_passwd[passwd_size + 10];
        char new_passwd[passwd_size];
	char crypt_passwd[passwd_size + 10];
	char cmd[100];
	const char* sat = "passwd";
	int length;

	if(get_user_passwd(args[1], args[2], old_passwd) == TRUE) {
		boolean found_passwd = FALSE;

		for(int i = 0; i < 3 && found_passwd == FALSE; i++) {
			printf("Old password : ");
                	fgets(new_passwd, passwd_size, stdin);
			//snprintf(crypt_passwd, strlen(crypt(new_passwd, sat)), "%s", crypt(new_passwd, sat));
			found_passwd = verif_passwd(old_passwd, new_passwd);
		}

		if(found_passwd == TRUE) {
			printf("New password : ");
			fgets(new_passwd, passwd_size, stdin);
			snprintf(crypt_passwd, passwd_size + 10, "%s", crypt(new_passwd, sat));
			length = (2 * strlen(args[2])) + strlen(old_passwd) + strlen(crypt_passwd) + strlen(args[1]) + 20;
			snprintf(cmd, length, "sed -i s/%s:%s/%s:%s/g %s", args[2], old_passwd, args[2], crypt_passwd, args[1]);
			//printf("cmd : %s \n", cmd);
			system(cmd);
		} else {
			printf("Error : Pasword incorrect \n");
			exit(EXIT_FAILURE);
		}
	} else {
		printf("New Password : ");
		fgets(new_passwd, passwd_size, stdin);
		//printf("%s \n", new_passwd);
		snprintf(crypt_passwd, passwd_size + 10, "%s", crypt(new_passwd, sat));
		length = strlen(args[2]) + strlen(crypt_passwd) + strlen(args[1]) + 20;
		snprintf(cmd, length, "echo %s:%s >> %s", args[2], crypt_passwd, args[1]);
		system(cmd);
	}

	exit(EXIT_SUCCESS);
}
