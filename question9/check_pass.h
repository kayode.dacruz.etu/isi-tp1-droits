#ifndef HEADER_FILE
#define HEADER_FILE

typedef enum {
	TRUE,
	FALSE
} boolean;

boolean get_user_passwd(char*, char*, char*);

void get_user_and_passwd(char*, char*, char*);

boolean verif_passwd(char*, char*);

#endif
