#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include "check_pass.h"

// faire un chown admin:admin rmg
// faire un chmod u+s rmg pour que n'importe quel utilisateur puisse etre comme un user admin pour supprimer
int rmg(char *filename) {
  char password[15];
  int gid = getgid();
  struct stat sb;
  register struct passwd *pw;
  
  if(stat(filename, &sb) == -1) {
    perror("stat");
    exit(EXIT_FAILURE);
  }
  
  if(sb.st_gid != gid){
    perror("Permission Denied for this file\n");
    exit(1);
  }

  printf("Enter your password: \n" );
  fgets(password, 15, stdin);
  pw = getpwuid(getuid());
 
  if(checkpass("passwd",pw->pw_name,password)==1) {
    if(remove(filename) != 0) {
      printf("FATAL: erreur suppression du fichier \n");
    } else {
      printf("\n#### fichier supprimé ###\n" );
    }
  } else { 
    exit(1);
  }

  exit(0);
}

int main(int argc, char  *argv[]) {
  rmg(argv[1]);
  return 0;
}
