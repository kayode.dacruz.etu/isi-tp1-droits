// Client side C/C++ program to demonstrate Socket programming 
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h>
#include "check_pass.h"
#define PORT 8080 

int main(int argc, char** argv)  { 
	if(argc != 2) {
		printf("usage : client [command file] \n");
		exit(EXIT_FAILURE);
	}

	int sock = 0, valread;
	int size = 20;	
	struct sockaddr_in serv_addr;
	char user[size];
	char passwd[size];	
	char cmd[size*2]; 
	char buffer[1024] = {0}; 
	
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
		printf("\n Socket creation error \n"); 
		return -1; 
	} 

	serv_addr.sin_family = AF_INET; 
	serv_addr.sin_port = htons(PORT); 
	
	// Convert IPv4 and IPv6 addresses from text to binary form 
	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) { 
		printf("\nInvalid address/ Address not supported \n"); 
		return -1; 
	} 

	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) { 
		printf("\nConnection Failed \n"); 
		return -1; 
	} 

	get_user_and_passwd(argv[1], user, passwd);
	snprintf(cmd, strlen(user) + strlen(passwd) + 5, "%s %s", user, passwd);
	send(sock, cmd, strlen(cmd), 0 );  
	valread = read(sock, buffer, 1024);

	if(strncmp("OK", buffer, strlen("OK")) == 0) {
		printf("Connection succes! \n");
		
		FILE* file;
		file = fopen(argv[1], "r");
        	if(file == NULL) {
                	printf("Error : Can't open file %s \n", argv[1]);
                	exit(EXIT_FAILURE);
        	}

        	char* line  = NULL;
        	size_t len = 0;
        	ssize_t r = getline(&line, &len, file);

        	while((r = getline(&line, &len, file)) != -1) {
                	snprintf(cmd, strlen(line) + 5, "%s", line);
					printf("cmd -> %s \n", cmd);
					send(sock, cmd, strlen(cmd), 0);

					valread = read(sock, buffer, 1024);
					
					while(strncmp("END", buffer, strlen("END")) != 0) {
						printf("%s", buffer);
						valread = read(sock, buffer, 1024);	
					}

					printf("End \n");
        	}

        	fclose(file);
		//valread = read(sock, buffer, 1024);
		//printf("%s\n", buffer);
	} else {
		printf("%s\n", buffer);
	}
	return 0; 
}
