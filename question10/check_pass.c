#include "check_pass.h"
#include <stdio.h>
#include <string.h>
#include <crypt.h>
#include <stdlib.h>

boolean get_user_passwd(char* path, char* user, char* passwd) {
	FILE* file;

        file = fopen(path, "r");
        if(file == NULL) {
                printf("Error : Can't open file %s \n", path);
                exit(EXIT_FAILURE);
        }

        char* line  = NULL;
        char* token = NULL;
        size_t len = 0;
        ssize_t read;

        while((read = getline(&line, &len, file)) != -1) {
		token = strtok(line, ":");
                if(strncmp(user, token, strlen(user)) == 0) {
                        token = strtok(NULL, ":");
                        snprintf(passwd, strlen(token), "%s", token);
                        fclose(file);
                        return TRUE;
                }
        }

        fclose(file);
        return FALSE;
}

void get_user_and_passwd(char* path, char* user, char* passwd) {
        FILE* file;

        file = fopen(path, "r");
        if(file == NULL) {
                printf("Error : Can't open file %s \n", path);
                exit(EXIT_FAILURE);
        }

        char* line = NULL;
        char* token = NULL;
        size_t len = 0;
        ssize_t read;
        if((read = getline(&line, &len, file)) != -1) {
                token = strtok(line, " ");
		snprintf(user, strlen(token) + 5, "%s", token); /* Copy user name in variable user */
		token = strtok(NULL, " ");
		snprintf(passwd, strlen(token) + 5, "%s", token); /* Copy user password in variable passwd */
                fclose(file);
                return;
        } else {
		fclose(file);
                printf("Error : Can't read line in file %s \n", path);
                exit(EXIT_FAILURE);
        }
}

boolean verif_passwd(char* old, char* new) {
	const char passwd[10] = "password";

        if (strncmp(old, crypt(new, "password"), strlen(old)) == 0) {
                return TRUE;
        }

        return FALSE;
}
