#include "cmd.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
static FILE *f;

FILE* launch(char* cmd, char* type) {
  //  FILE* f = malloc(sizeof(FILE));

    f = popen(cmd, type);

    if(f == NULL) {
        printf("Error : popen \n");
        exit(EXIT_FAILURE);
    }

    return f;
}

void send_cmd_answer(FILE* f, int socket) {
    char line[200];

    printf("Hello \n");
    while(fgets(line, sizeof line, f)) {
        //printf("Here \n");
        //send(socket, line, strlen(line), 0);
        send(socket, "Hello boy\n", strlen("Hello boy\n"), 0);
    }

    send(socket, "END", strlen("END"), 0);

    pclose(f);
}

/*int main(void) {
    FILE* f = NULL;

    f = launch("ls -la ./", "r");
    print_cmd_answer(f);

    exit(EXIT_SUCCESS);
}*/
