// Server side C/C++ program to demonstrate Socket programming 
#include "check_pass.h"
#include "cmd.h"
#include <unistd.h> 
#include<sys/wait.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <sys/types.h>
#include <pwd.h>
#define PORT 8080 

char cmd_args[30];
char line[500];
FILE* f;

void exec(char* cmd, char* args, int socket) {
	//static char cmd_args[30];

	if(strncmp("list", cmd, strlen("list")) == 0) {
		snprintf(cmd_args, strlen(args) + 10, "ls %s", args);
		printf("real cmd -> %s \n", cmd_args);
	}
	
	if(strncmp("read", cmd, strlen("read")) == 0) {
		snprintf(cmd_args, strlen(args) + 10, "cat %s", args);
		printf("real cmd -> %s \n", cmd_args);
	}

	//static FILE* f;
	f = popen(cmd_args, "r");

	if(f == NULL) {
		printf("Error : popen \n");
		exit(EXIT_FAILURE);
	}

	while(fgets(line, sizeof line, f) != NULL) {
		send(socket, line, strlen(line), 0);
	}

	send(socket, "END", strlen("END"), 0);

	pclose(f);
}

int main(int argc, char** argv)  {
	if(argc != 2) {
		printf("usage : server [password file] \n");
		exit(EXIT_FAILURE);
	}

	int server_fd, new_socket, valread; 
	struct sockaddr_in address; 
	int opt = 1; 
	int addrlen = sizeof(address); 
	char buffer[1024] = {0};  
	
	// Creating socket file descriptor 
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) { 
		perror("socket failed"); 
		exit(EXIT_FAILURE); 
	} 
	
	// Forcefully attaching socket to the port 8080 
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) { 
		perror("setsockopt"); 
		exit(EXIT_FAILURE); 
	} 

	address.sin_family = AF_INET; 
	address.sin_addr.s_addr = INADDR_ANY; 
	address.sin_port = htons( PORT ); 
	
	// Forcefully attaching socket to the port 8080 
	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) { 
		perror("bind failed"); 
		exit(EXIT_FAILURE); 
	} 

	if (listen(server_fd, 3) < 0) { 
		perror("listen"); 
		exit(EXIT_FAILURE); 
	} 

	printf("Listen... \n\n");

	while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) >= 0) { 
		valread = read(new_socket, buffer, 1024);
        
        int size = 20;
        char* token = NULL;
        char user[size];
        char old_passwd[size];
        char passwd[size];

        token = strtok(buffer, " ");
        snprintf(user, strlen(token) + 5, "%s", token);
        token = strtok(NULL, " ");
        snprintf(passwd, strlen(token) + 5, "%s", token);
        	
        if(get_user_passwd(argv[1], user, old_passwd) == TRUE) {
			if(verif_passwd(old_passwd, passwd) == TRUE) {
				send(new_socket, "OK", strlen("OK"), 0);

				if(fork() == 0) {
					char cmd[5];
					char args[20];
					boolean close = FALSE;


					printf("EUID : %d - EGID : %d \n", geteuid(), getegid());
					struct passwd* p_user = getpwnam(user);

					if(seteuid(p_user->pw_uid) == -1) {
						perror("seteuid");
						exit(EXIT_FAILURE);
					}

					if(setegid(p_user->pw_gid) == -1) {
						perror("seteuid");
						exit(EXIT_FAILURE);
					}

					printf("EUID : %d - EGID : %d \n", geteuid(), getegid());
						
					while((valread = read(new_socket, buffer, 1024)) > 0 && close == FALSE) {
						printf("client cmd -> %s \n", buffer);
						token = strtok(buffer, " ");
						snprintf(cmd, strlen(token) + 5, "%s", token);
						token = strtok(NULL, " ");
						snprintf(args, strlen(token) + 5, "%s", token);
						exec(cmd, args, new_socket);
					}

					exit(EXIT_SUCCESS);
				} else {
					wait(NULL);
				}
			} else {
				printf("KO : Connection failed for %s (user)\n", user);
				send(new_socket, "KO : Connection failed!", strlen("KO : Connection failed!"), 0);	
			}
        } else {
			send(new_socket, "KO : Connection failed!", strlen("KO : Connection failed!"), 0);
		}
	}

	perror("accept");
        exit(EXIT_FAILURE);
} 

