# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- DA CRUZ Kayode, email: kayode.dacruz.etu@univ-lille.fr

- ADAOUN Rayane, email: rayane.adaoun.etu@univ-lille.fr

## Question 1

sudo adduser toto
sudo adduser toto ubuntu

Le processus (créer par toto) ne pourra pas écrire sur le fichier parce que l'utilisateur toto n'a pas le droit en écriture sur le fichier. 

## Question 2

La caractère x signifie qu'on peut accéder à un répertoire.
L'utilisateur toto ne peut pas entrer dans le répertoire parce que le groupe ubuntu auquelle il appartient n'a pas droit en exécution (x) sur le dossier.
L'utilisateur toto peut affciher le contenur du répertoire mais pas les droits d'accès des différent fichier qu'il contient. Parce qu'il appartient au groupe ubuntu qui n'a pas droit d'exécution sur le dossier.

## Question 3

Avant le set-user-id les valeurs des ids sont égalent à celle de toto.

[toto@machine-4 centos]$ ./main mydir/data.txt 
getuid: 1001
getgid: 1001
geteuid: 1001
getegid: 1001
Hello world
Cannot open file: Permission denied 

Après la set-user-id les valeurs les ids rélle(RUID & RGID) restent égale à celle de toto. Par contre les ids éffectifs(EUID & EGID) sont celle du groupe ubuntu.

[toto@machine-4 centos]$ ./main mydir/data.txt 
getuid: 1001
getgid: 1001
geteuid: 1000
getegid: 1000
Hello world
File opens correctly

## Question 4

Les valeurs des ids sont égalent à celle de toto meme àprès avoir activé le set-user-id

## Question 5

La commande chfn permet de modifier les paramètres des comptes de  l'utilisateur qui l'exécute.


## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier /etc/shadow.
Ce fichier est uniquement modifiable par l'executable /urs/bin/passwd. Ce exécutable quant à lui ne peut etre lancer que par le root.

[toto@machine-4 mydir]$ ll /etc/shadow
---------- 1 root root 808 13 janv. 18:15 /etc/shadow
root@machine-4 mydir]# ll /usr/bin/passwd 
-rwsr-xr-x. 1 root root 27832 10 juin   2014 /usr/bin/passwd
[toto@machine-4 mydir]$ passwd toto
passwd : Seul le super-utilisateur peut indiquer un nom d'utilisateur

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








